const tabTitles = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tab-content');
let selectedTab = localStorage.getItem('selectedTab') || 'web-design';

function setActiveTab(tab) {
  tabTitles.forEach(tabTitle => tabTitle.classList.remove('active'));
  document.querySelector(`[data-tab="${tab}"]`).classList.add('active');
  tabContents.forEach(content => {
    const contentTab = content.dataset.tab;
    if (contentTab === tab) {
      content.style.display = 'block';
    } else {
      content.style.display = 'none';
    }
  });
  localStorage.setItem('selectedTab', tab);
}
function handleTabClick(event) {
  const clickedTab = event.target.dataset.tab;
  setActiveTab(clickedTab);
}
tabTitles.forEach(tabTitle => {
  tabTitle.addEventListener('click', handleTabClick);
});
setActiveTab(selectedTab);
const tabs = document.querySelectorAll('.tabs-title');
tabs.forEach(tab => {
  tab.addEventListener('click', () => {
    tabs.forEach(tab => tab.classList.remove('active'));
    tab.classList.add('active');
  });
});







document.addEventListener('DOMContentLoaded', () => {
  const tabs = document.querySelectorAll('.tabs-amazing');
  const gridItems = document.querySelectorAll('.amazing-work__grid > div');
  const loadMoreBtn = document.getElementById('load-more-btn');
  const loader = document.querySelector('.loader');
  const imagesToAdd = 12;

  tabs.forEach((tab) => {
    tab.addEventListener('click', () => {
      const filter = tab.dataset.filter;

      gridItems.forEach((item, index) => {
        const itemFilters = item.classList;

        if (filter === 'all') {
          if (index < imagesToAdd) {
            item.classList.remove('hidden');
          } else {
            item.classList.add('hidden');
          }
        } else if (itemFilters.contains(filter)) {
          item.classList.remove('hidden');
        } else {
          item.classList.add('hidden');
        }
      });

      tabs.forEach((tab) => {
        tab.classList.remove('active');
      });

      tab.classList.add('active');
      loadMoreBtn.style.display = 'block';

      if (filter === 'all') {
        loadMoreBtn.style.display = 'block';
      } else {
        loadMoreBtn.style.display = 'none';
      }
    });
  });

  loadMoreBtn.addEventListener('click', () => {
    const hiddenItems = document.querySelectorAll('.amazing-work__grid > div.hidden');
    const visibleItems = Array.from(hiddenItems).slice(0, imagesToAdd);

    loader.style.display = 'block';
    loadMoreBtn.style.display = 'none';

    setTimeout(() => {
      visibleItems.forEach((item) => {
        item.classList.remove('hidden');
      });

      if (hiddenItems.length <= imagesToAdd) {
        loadMoreBtn.style.display = 'none';
      } else {
        loadMoreBtn.style.display = 'block';
      }

      loader.style.display = 'none';
    }, 2000);
  });
});






document.addEventListener('DOMContentLoaded', () => {
  const carouselItems = document.querySelectorAll('.carousel-item');
  const separateImg = document.querySelector('.separate-img');
  const separateImgText = separateImg.querySelector('.separate-img-text');
  const separateImgHeading = separateImg.querySelector('.separate-img-heading');
  const separateImgDescription = separateImg.querySelector('.separate-img-description');
  const prevControl = document.querySelector('.carousel-control.prev');
  const nextControl = document.querySelector('.carousel-control.next');
  let activeIndex = 0;

  function showSlide(index) {
    const clickedItem = carouselItems[index];
    const imgUrl = clickedItem.querySelector('img').src;
    const text = clickedItem.querySelector('img').getAttribute('data-text');
    const heading = clickedItem.querySelector('img').getAttribute('data-heading');
    const description = clickedItem.querySelector('img').getAttribute('data-description');

    separateImg.style.backgroundImage = `url(${imgUrl})`; 
    separateImgText.textContent = text;
    separateImgHeading.textContent = heading;
    separateImgDescription.textContent = description;

    separateImg.style.animation = 'none'; 
    separateImg.offsetHeight; 
    separateImg.style.animation = 'flipAnimation 2.5s forwards'; 

    separateImg.classList.remove('active'); 
    separateImg.style.transform = 'none'; 

    carouselItems.forEach((item) => {
      item.classList.remove('active');
      item.style.transform = 'none';
    });

    clickedItem.classList.add('active');
    clickedItem.style.transform = 'translateY(-10px)';
  }

  function showNextSlide() {
    activeIndex++;
    if (activeIndex >= carouselItems.length) {
      activeIndex = 0;
    }
    showSlide(activeIndex);
  }

  function showPreviousSlide() {
    activeIndex--;
    if (activeIndex < 0) {
      activeIndex = carouselItems.length - 1;
    }
    showSlide(activeIndex);
  }

  carouselItems.forEach((item, index) => {
    item.addEventListener('click', () => {
      showSlide(index);
    });
  });

  prevControl.addEventListener('click', () => {
    showPreviousSlide();
  });

  nextControl.addEventListener('click', () => {
    showNextSlide();
  });

  showSlide(activeIndex);
});










